# Project title setter

Project title setter is een python script om RBV tabellen machine leesbaar te maken.
Het werkt bij tabellen die volgens Model 2.21 - Departementale begrotingsstaat ingevuld zijn.


## Installatie
Gebruik Poetry (https://python-poetry.org/) om title setter te installeren:
```
 poetry add git+https://gitlab.com/azartoosa/title_setter
```
Of installeer de module vanaf gitlab:
```
pip install git+https://gitlab.com/azartoosa/title_setter
```

## Contact

Author: Ariane Azartoos (azartoosa@gmail.com)
